<?
function common_chart_css_js()
{?>
    <link rel="stylesheet" href="<?=plugin_dir_url(__FILE__)?>css/all.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?=plugin_dir_url(__FILE__)?>css/adminlte.min.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <script src="<?=plugin_dir_url(__FILE__)?>js/jquery.min.js"></script>
    <script src="<?=plugin_dir_url(__FILE__)?>js/bootstrap.bundle.min.js"></script>
    <script src="<?=plugin_dir_url(__FILE__)?>js/Charts.min.js"></script>
    <script src="<?=plugin_dir_url(__FILE__)?>js/adminlte.min.js"></script>
    <script src="<?=plugin_dir_url(__FILE__)?>js/demo.js"></script>
<?}
add_shortcode('common_chart_css_js', 'common_chart_css_js');