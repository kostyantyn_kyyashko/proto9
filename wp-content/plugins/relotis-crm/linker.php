<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
$transaction_id = $_GET['transaction_id']; echo $transaction_id; die();
$wp_user_id = $_GET['wp_user_id'];
$wp_user_name = $_GET['wp_user_name'];
$stage = $_GET['status'];
setcookie('transaction_id', $transaction_id, time() + 7*3600, '/');
setcookie('wp_user_id', $wp_user_id, time() + 7*3600, '/');
setcookie('wp_user_name', $wp_user_name, time() + 7*3600, '/');
setcookie('stage', $stage, time() + 7*3600, '/');
setcookie('div_stage_button_id', div_stage_button_id($stage), time() + 7*3600, '/');

function div_stage_button_id($stage)
{
    $div_stage_button_id = 0;
    switch ($stage) {
        case 'Analysis':
            $div_stage_button_id = 'jet-tabs-control-1001';
            break;
        case 'Contacts':
            $div_stage_button_id = 'jet-tabs-control-1002';
            break;
        case 'Proposal':
            $div_stage_button_id = 'jet-tabs-control-1003';
            break;
        case 'Negotiations':
            $div_stage_button_id = 'jet-tabs-control-1004';
            break;
        case 'Close':
            $div_stage_button_id = 'jet-tabs-control-1005';
            break;
    }
    return $div_stage_button_id;
}

header('Location: /ulcrm');
